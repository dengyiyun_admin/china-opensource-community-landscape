# **WeDataSphere (WDS) 开源项目生态**

# **WeDataSphere** 社区现状


WeDataSphere 一站式、金融级、全连通、开源开放的大数据平台套件，2016 年至今，WeDataSpher 从底层的基础引擎引入开始，逐步完成了上层功能工具的构建，及中间层中间件的连通集成和治理管控。从 2019 年对外开源以来，面向社区已经发布了 9 个开源组件，填补了业界 “开源体系化大数据平台套件”的空白，受到了各行业的广泛欢迎和采用。

![image.png](https://s2.loli.net/2022/07/19/4JIonyU9dmNKE1f.png)


截止 2022年 6 月，沙箱用户超 1600 个，自建试用企业超 800 家，收到超 80 家企业投入生产的反馈，生产环境支撑的数据量达 400 PB，生产用户超 5,000 ，开源社区成员人数近 7,000 ；涉及金融、互联网、通信、制造、教育等众多行业。其中，计算中间件 Linkis 已进入国际开源基金会 Apache 孵化，是 WeDataSphere 平台中发展较为成熟且应用最受广泛的组件。



# **关于 Apache Linkis**


![image.png](https://s2.loli.net/2022/07/19/rX5N9RjBiVLEYG2.png)


Apache Linkis 是大数据平台中间件服务的集合，致力于在上层应用和底层引擎之间构建一层计算中间件。使用 Linkis 提供的 REST/WebSocket/JDBC 等标准接口，上层应用能方便地连接访问 Spark, Presto, Flink 等底层引擎,实现跨引擎上下文共享、统一的计算任务和引擎治理与编排能力，更多内容请访问 [linkis.apache.org](https://linkis.apache.org)


## **Linkis 计算中间件层概念图**


![输入图片说明](images/linkismiddlewareimage.png)


## **Linkis 功能架构**

![image.png](https://s2.loli.net/2022/07/19/O2GN4UzP3gX7FwD.png)

 

## **Linkis 技术架构**

![image.png](https://s2.loli.net/2022/07/19/TGuJobitqHLYWNP.png)




# **社区治理模式**

Apache Linkis 是微众银行大数据团队自主研发的中间件项目，并在社区不断逐步完善，已经形成了完备的管理体制。社区维护和管理成员目前主要由微众银行大数据团队成员和部分社区核心开发人员负责，未来，将会引入更多社区力量一起管理和运营 Apache Linkis 社区，逐步扩大社区的规模和影响力。

# **运营实践**

1.社区用户方面：截至 2021 年 7 月整个社区成员已至近 5,000 余人，Linkis 团队也积极同社区用户一起共建，目前核心开发及社区维护团队人数至 60 余人，并在社区日常运营过程中，收到了大量来自社区用户的补丁修复、性能优化、特性增强补丁的提交请求。

2.社区运营方面：除了日常的社群互动， Linkis 社区还成功举办多次线上线下 meetup，涵盖了案例分享，技术分享，与其它社区深度合作案例分享等多个主题，吸引更多社区用户参与 Linkis 及 WeDataSphere 平台的共建。

3.企业用户覆盖电信、金融、互联网、制造、零售、教育等各行各业，其中，与电信天翼云、Boss 直聘、萨摩耶云、仙翁科技、平安保险、蔚来汽车、招联金融等进行了深度合作。WeDataSphere 大数据解有 50 余家企业将 WeDataSphere 平台运用到实际生产业务中决方案平台致力于推动社会各行各业的对数据生产要素的利用，为普罗大众创造价值。

 
  ![image.png](https://s2.loli.net/2022/07/19/2PZLi6vwbxMjVRa.png)


# 发展路径

## 决策诞生过程

最初 Linkis 是内部自研的 Remote Server 工具，只是用来解决 WebIDE  单一工具的任务高并发和多租户隔离问题。大家在使用它的过程中，逐渐对它提出越来越多的需求，希望可以打通开发工具、调度系统后台，对接多个上层功能工具，统一管理任务的提交、解析和执行。

于是这款 Remote Server 演变为了统一任务执行服务 UJES(Unified Job Execution Service)，接下来技术团队继续完成了更多功能的“填空题”， 逐步完善了 UJES 的连通、管控相关能力，包括数据脱敏、数据质量校验、可视化等更多功能工具系统与 UJES 的对接。至此 UJES 已基本具备大数据平台中间件的连通、管控、复用相关核心功能，Linkis 雏形初现。

 

## 项目里程碑事件

  Linkis 近 3 年一路走来，经过了社区，企业及市场的验证和广泛认可。 


![image.png](https://s2.loli.net/2022/07/19/pb52aPLFlEJ9XfY.png)


## 未来规划

 Linkis 项目在社区中的发展态势良好， 同时，我们也希望给大家带来更更多的帮助将 Linkis 一起打磨的更好。
 ![输入图片说明](images/linkisfutureimage.png)
  

# WeDataSphere 生态组件项目仓库地址

### linkis 计算中间件

[https://github.com/WeBankFinTech/WeDataSphere](https://github.com/WeBankFinTech/WeDataSphere)

[https://gitee.com/WeBank/Linkis](https://gitee.com/WeBank/Linkis)


### DataSphereStudio 一站式数据应用开发管理门户

[https://github.com/WeBankFinTech/DataSphereStudio](https://github.com/WeBankFinTech/DataSphereStudio)

[https://gitee.com/WeBank/DataSphereStudio](https://gitee.com/WeBank/DataSphereStudio)


### Qualitis 数据质量管理平台

[https://github.com/WeBankFinTech/Qualitis](https://github.com/WeBankFinTech/Qualitis)

[https://gitee.com/WeBank/Qualitis](https://gitee.com/WeBank/Qualitis)


### Schedulis 工作流任务调度系统

[https://github.com/WeBankFinTech/Schedulis](https://github.com/WeBankFinTech/Schedulis)

[https://gitee.com/WeBank/Schedulis](https://gitee.com/WeBank/Schedulis)


### Exchangis 数据交换平台

[https://github.com/WeBankFinTech/Exchangis](https://github.com/WeBankFinTech/Exchangis)

[https://gitee.com/WeBank/Exchangis](https://gitee.com/WeBank/Exchangis)


### Prophecis 一站式机器学习平台

[https://github.com/WeBankFinTech/Prophecis](https://github.com/WeBankFinTech/Prophecis)

[https://gitee.com/WeBank/Prophecis](https://gitee.com/WeBank/Prophecis)


### Scriptis 交互式数据分析Web工具

[https://github.com/WeBankFinTech/Scriptis](https://github.com/WeBankFinTech/Scriptis)

[https://gitee.com/WeBank/Scriptis](https://gitee.com/WeBank/Scriptis)


### Visualis 数据可视化工具

[https://github.com/WeBankFinTech/Visualis](https://github.com/WeBankFinTech/Visualis)

[https://gitee.com/WeBank/Visualis](https://gitee.com/WeBank/Visualis)



