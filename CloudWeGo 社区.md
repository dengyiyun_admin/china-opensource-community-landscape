### CloudWeGo 
website: https://www.cloudwego.io/ 
![cloudwego ](https://user-images.githubusercontent.com/104130938/208568174-b2a11090-6872-46b6-87cf-a772be51ba18.png)

### 项目介绍
CloudWeGo 是一套由字节跳动开源的、可快速构建企业级云原生微服务架构的中间件集合。CloudWeGo 项目共同的特点是高性能、高扩展性、高可靠，专注于微服务通信与治理。

## 项目组成
<img width="687" alt="image" src="https://user-images.githubusercontent.com/104130938/208567876-f6db8f59-4e0f-4d97-aa4e-0b11f3b84dda.png">

## 项目发展时间线
<img width="707" alt="image" src="https://user-images.githubusercontent.com/104130938/208567947-0c52f1ff-1b2b-47ca-b5c4-38642f3d09e0.png">

### 项目用户
![image](https://user-images.githubusercontent.com/104130938/208568721-28068e43-d296-497d-bdee-c42528ad76a7.png)

### 项目成就
<img width="665" alt="image" src="https://user-images.githubusercontent.com/104130938/208568686-52e9a6b1-4bb4-426b-bd3a-a69b44fb49f2.png">

